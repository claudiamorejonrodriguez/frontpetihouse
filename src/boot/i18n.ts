import boot from "@/wrappers/index";
import { createI18n } from "vue-i18n";

import messages from "@/i18n";

const i18n = createI18n({
  locale: "en-US",
  fallbackLocale: "en-US",
  messages,
});

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
export default boot((app) => {
  // Set i18n instance on app
  app.use(i18n);
  return app;
});

export { i18n };
