// This is just an example,
// so you can safely delete all default props below

export default {
  success: "Action was successful",
  about: "About",
  pets: "Pets",
  contact: "Contact",
  donation: "Donation",
  petRegister: "Pet Register",
  admUsers: "Adm Users",
  login: "Login",
  register: "Register",
  profile: "Profile",
  logOut: "Log Out",
};
