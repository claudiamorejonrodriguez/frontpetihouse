// This is just an example,
// so you can safely delete all default props below

export default {
  success: "Action was successful",
  about: "Acerca De",
  pets: "Mascotas",
  contact: "Contacto",
  donation: "Donacion",
  petRegister: "Registrar Mascota",
  admUsers: "Adm Usuarios",
  login: "Iniciar Sesion",
  register: "Registro",
  profile: "Perfil",
  logOut: "Salir",
};
