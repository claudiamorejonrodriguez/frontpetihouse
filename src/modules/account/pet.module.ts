import { reactive, toRefs } from "vue";
import { IPet, IPetStatus } from "@/modules/account/models";
import { api } from "@/boot/axios";
import useAccount from "@/modules/account/account.module";

const state: IPetStatus = reactive({
  error: null,
  loading: false,
});

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export default function usePet() {
  const doRegister = (data: IPet): Promise<void> => {
    state.loading = true;
    const formData = new FormData();
    // @ts-ignore
    Object.keys(data).forEach((key) => formData.append(key, data[key]));
    const { person } = useAccount();
    return api
      .post(`/pet/register`, formData, {
        headers: {
          authorization: `${person?.value?.accessToken}`,
        },
      })
      .then((value) => {
        state.error = null;
      })
      .catch((reason) => {
        if (reason.response) {
          state.error = reason.response.data;
        }
      })
      .finally(() => {
        state.loading = false;
      });
  };

  const doUpdate = (data: IPet): Promise<void> => {
    state.loading = true;
    const formData = new FormData();
    // @ts-ignore
    Object.keys(data).forEach((key) => formData.append(key, data[key]));
    const { person } = useAccount();
    return api
      .put(`/pet/update/${data.id}`, formData, {
        headers: {
          authorization: `${person?.value?.accessToken}`,
        },
      })
      .then((value) => {
        state.error = null;
      })
      .catch((reason) => {
        if (reason.response) {
          state.error = reason.response.data;
        }
      })
      .finally(() => {
        state.loading = false;
      });
  };

  const doGetMePets = (data: any): Promise<IPet[]> => {
    state.loading = true;
    const { person } = useAccount();
    return api
      .get(`/pet/list/me`, {
        params: data,
        headers: {
          authorization: `${person?.value?.accessToken}`,
        },
      })
      .then((value) => {
        state.error = null;
        return value.data.pets;
      })
      .catch((reason) => {
        if (reason.response) {
          state.error = reason.response.data;
        }
      })
      .finally(() => {
        state.loading = false;
      });
  };

  const doGetAdminPets = (data: any): Promise<IPet[]> => {
    state.loading = true;
    const { person } = useAccount();
    return api
      .get(`/pet/list/admin`, {
        params: data,
        headers: {
          authorization: `${person?.value?.accessToken}`,
        },
      })
      .then((value) => {
        state.error = null;
        return value.data.pets;
      })
      .catch((reason) => {
        if (reason.response) {
          state.error = reason.response.data;
        }
      })
      .finally(() => {
        state.loading = false;
      });
  };

  const doGetPets = (): Promise<IPet[]> => {
    state.loading = true;
    return api
      .get(`/pet/list`)
      .then((value) => {
        state.error = null;
        return value.data.pets;
      })
      .catch((reason) => {
        if (reason.response) {
          state.error = reason.response.data;
        }
      })
      .finally(() => {
        state.loading = false;
      });
  };

  const doGetPetProfile = (data: string): Promise<IPet> => {
    state.loading = true;
    return api
      .get(`/pet/${data}`)
      .then((value) => {
        state.error = null;
        return value.data;
      })
      .catch((reason) => {
        if (reason.response) {
          state.error = reason.response.data;
        }
      })
      .finally(() => {
        state.loading = false;
      });
  };

  const doPetDenied = (data: any): Promise<IPet> => {
    state.loading = true;
    const { person } = useAccount();
    return api
      .post(
        `/pet/${data.id}/denied`,
        { person: data.person },
        {
          headers: {
            authorization: `${person?.value?.accessToken}`,
          },
        }
      )
      .then((value) => {
        state.error = null;
        return value.data;
      })
      .catch((reason) => {
        if (reason.response) {
          state.error = reason.response.data;
        }
      })
      .finally(() => {
        state.loading = false;
      });
  };

  const doPetInterested = (data: any): Promise<IPet> => {
    state.loading = true;
    const { person } = useAccount();
    return api
      .post(`/pet/${data.id}/interested`, null, {
        headers: {
          authorization: `${person?.value?.accessToken}`,
        },
      })
      .then((value) => {
        state.error = null;
        return value.data;
      })
      .catch((reason) => {
        if (reason.response) {
          state.error = reason.response.data;
        }
      })
      .finally(() => {
        state.loading = false;
      });
  };

  const doPetActive = (data: any): Promise<IPet> => {
    state.loading = true;
    const { person } = useAccount();
    return api
      .post(`/pet/${data.id}/active`, null, {
        headers: {
          authorization: `${person?.value?.accessToken}`,
        },
      })
      .then((value) => {
        state.error = null;
        return value.data;
      })
      .catch((reason) => {
        if (reason.response) {
          state.error = reason.response.data;
        }
      })
      .finally(() => {
        state.loading = false;
      });
  };

  const doPetAccepted = (data: any): Promise<IPet> => {
    state.loading = true;
    const { person } = useAccount();
    return api
      .post(
        `/pet/${data.id}/accepted`,
        { person: data.person },
        {
          headers: {
            authorization: `${person?.value?.accessToken}`,
          },
        }
      )
      .then((value) => {
        state.error = null;
        return value.data;
      })
      .catch((reason) => {
        if (reason.response) {
          state.error = reason.response.data;
        }
      })
      .finally(() => {
        state.loading = false;
      });
  };

  return {
    ...toRefs(state),
    doRegister,
    doGetPets,
    doGetPetProfile,
    doUpdate,
    doPetDenied,
    doPetAccepted,
    doPetInterested,
    doGetMePets,
    doGetAdminPets,
    doPetActive,
  };
}
