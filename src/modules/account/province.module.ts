import { reactive, toRefs } from "vue";
import { api } from "@/boot/axios";

const state = reactive({
  error: null,
  loading: false,
});

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export default function useProvince() {
  const doGetAllProvince = (): Promise<any> => {
    state.loading = true;
    return api
      .get(`/province`)
      .then((value) => {
        state.error = null;
        return value.data;
      })
      .catch((reason) => {
        if (reason.response) {
          state.error = reason.response.data;
        }
        return false;
      })
      .finally(() => {
        state.loading = false;
      });
  };

  const doGetProvince = (data: any): Promise<any> => {
    state.loading = true;
    return api
      .get(`/province/${data}`)
      .then((value) => {
        state.error = null;
        return value.data;
      })
      .catch((reason) => {
        if (reason.response) {
          state.error = reason.response.data;
        }
        return false;
      })
      .finally(() => {
        state.loading = false;
      });
  };

  return {
    ...toRefs(state),
    doGetAllProvince,
    doGetProvince,
  };
}
