import { reactive, toRefs } from "vue";
import {
  ILogin,
  IPassword,
  IPerson,
  IPersonStatus,
  IRecovery,
  IRegister,
  ROLES,
} from "@/modules/account/models";
import { api } from "@/boot/axios";

const state: IPersonStatus = reactive({
  error: null,
  person: null,
  loading: false,
});

if (localStorage.getItem("user")) {
  state.person = JSON.parse(<string>localStorage.getItem("user"));
  const { doValidate } = useAccount();
  doValidate()
    .then((value) => {
      if (!value) {
        localStorage.removeItem("user");
        return (state.person = null);
      }
    })
    .catch((reason) => {
      state.error = reason;
    });
}

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export default function useAccount() {
  const doLogin = (data: ILogin): Promise<void> => {
    state.loading = true;
    return api
      .post(`/auth/token`, data)
      .then((value) => {
        localStorage.setItem("user", JSON.stringify(value.data));
        state.person = value.data;
        state.error = null;
      })
      .catch((reason) => {
        if (reason.response) {
          state.error = reason.response.data;
        }
      })
      .finally(() => {
        state.loading = false;
      });
  };

  const doRegisterAttempt = (data: IRegister): Promise<void> => {
    state.loading = true;
    return api
      .post(`/account/register/attempt`, data)
      .then((value) => {
        state.person = value.data;
        state.error = null;
      })
      .catch((reason) => {
        if (reason.response) {
          state.error = reason.response.data;
        }
      })
      .finally(() => {
        state.loading = false;
      });
  };

  const doRecovery = (data: IRecovery): Promise<void> => {
    state.loading = true;
    return api
      .get(`/account/exists?email=${data.email}`)
      .then((value) => {
        if (value && value.data && value.data.exist) {
          api
            .post(`/account/pwd/recover/attempt`, data)
            .then((value) => {
              state.person = value.data;
              state.error = null;
            })
            .catch((reason) => {
              if (reason.response) {
                state.error = reason.response.data;
              }
            })
            .finally(() => {
              state.loading = false;
            });
        }
        state.error = null;
      })
      .catch((reason) => {
        if (reason.response) {
          state.error = reason.response.data;
        }
      })
      .finally(() => {
        state.loading = false;
      });
  };

  const doConfirm = (data: string): Promise<void> => {
    state.loading = true;
    return api
      .post(`/auth/method/confirm`, null, {
        headers: {
          authorization: `Bearer ${data}`,
        },
      })
      .then((value) => {
        localStorage.setItem("user", JSON.stringify(value.data));
        state.person = value.data;
        state.error = null;
      })
      .catch((reason) => {
        if (reason.response) {
          state.error = reason.response.data;
        }
      })
      .finally(() => {
        state.loading = false;
      });
  };

  const doChangePassword = (data: IPassword): Promise<void> => {
    state.loading = true;
    return api
      .post(`/account/pwd/recover/accept`, data, {
        headers: {
          authorization: `${state.person?.accessToken}`,
        },
      })
      .then((value) => {
        state.error = null;
      })
      .catch((reason) => {
        if (reason.response) {
          state.error = reason.response.data;
        }
      })
      .finally(() => {
        state.loading = false;
      });
  };

  const doValidate = (): Promise<boolean> => {
    state.loading = true;
    return api
      .get(`/auth/validate`, {
        headers: {
          authorization: `${state.person?.accessToken}`,
        },
      })
      .then((value) => {
        state.error = null;
        return value.data;
      })
      .catch((reason) => {
        if (reason.response) {
          state.error = reason.response.data;
        }
        return false;
      })
      .finally(() => {
        state.loading = false;
      });
  };

  const doLogOut = (): Promise<void> => {
    state.loading = true;
    return new Promise<void>((resolve, reject) => {
      if (isAuthenticated() && localStorage.getItem("user")) {
        localStorage.removeItem("user");
        state.person = null;
        state.error = null;
        state.loading = false;
        resolve();
      } else {
        state.loading = false;
        reject({ error: true });
      }
    });
  };

  const doUpdatePersonProfile = (data: IPerson): Promise<void> => {
    state.loading = true;
    return api
      .put(`/account/profile`, data, {
        headers: {
          authorization: `${state.person?.accessToken}`,
        },
      })
      .then((value) => {
        state.person = JSON.parse(<string>localStorage.getItem("user"));
        (state.person as any).person = value.data;
        localStorage.setItem("user", JSON.stringify(state.person));
        state.error = null;
      })
      .catch((reason) => {
        if (reason.response) {
          state.error = reason.response.data;
        }
      })
      .finally(() => {
        state.loading = false;
      });
  };

  const doGetPersons = (): Promise<IPerson[]> => {
    state.loading = true;
    return api
      .get(`/account`, {
        headers: {
          authorization: `${state.person?.accessToken}`,
        },
      })
      .then((value) => {
        state.error = null;
        return value.data;
      })
      .catch((reason) => {
        if (reason.response) {
          state.error = reason.response.data;
        }
      })
      .finally(() => {
        state.loading = false;
      });
  };

  const doImageChange = (data: any): Promise<void> => {
    state.loading = true;
    const formData = new FormData();
    formData.append("image", data as any);
    return api
      .post(`/account/image`, formData, {
        headers: {
          authorization: `${state.person?.accessToken}`,
        },
      })
      .then((value) => {
        state.person = JSON.parse(<string>localStorage.getItem("user"));
        (state.person as any).person = value.data;
        localStorage.setItem("user", JSON.stringify(state.person));
        state.error = null;
      })
      .catch((reason) => {
        if (reason.response) {
          state.error = reason.response.data;
        }
      })
      .finally(() => {
        state.loading = false;
      });
  };

  const doRolesChange = (data: any): Promise<void> => {
    state.loading = true;
    return api
      .put(
        `/rol/${data.id}`,
        { roles: data.roles },
        {
          headers: {
            authorization: `${state.person?.accessToken}`,
          },
        }
      )
      .then((value) => {
        state.error = null;
      })
      .catch((reason) => {
        if (reason.response) {
          state.error = reason.response.data;
        }
      })
      .finally(() => {
        state.loading = false;
      });
  };

  const doPWROverride = (data: any): Promise<void> => {
    state.loading = true;
    return api
      .post(
        `/account/${data.id}/pwd/override`,
        { password: data.password },
        {
          headers: {
            authorization: `${state.person?.accessToken}`,
          },
        }
      )
      .then((value) => {
        state.error = null;
      })
      .catch((reason) => {
        if (reason.response) {
          state.error = reason.response.data;
        }
      })
      .finally(() => {
        state.loading = false;
      });
  };

  const doChangeStatus = (data: any): Promise<void> => {
    state.loading = true;
    return api
      .put(
        `/account/${data.id}/status/change`,
        { status: data.status },
        {
          headers: {
            authorization: `${state.person?.accessToken}`,
          },
        }
      )
      .then((value) => {
        state.error = null;
      })
      .catch((reason) => {
        if (reason.response) {
          state.error = reason.response.data;
        }
      })
      .finally(() => {
        state.loading = false;
      });
  };

  const isAuthenticated = () => {
    return !!state.person?.accessToken;
  };

  const isAdmin = () => {
    return !!(
      state.person?.person.roles &&
      state.person?.person.roles?.indexOf(ROLES.ADMIN) >= 0
    );
  };

  return {
    ...toRefs(state),
    isAuthenticated,
    doLogin,
    doValidate,
    doLogOut,
    doRegisterAttempt,
    doConfirm,
    doRecovery,
    doChangePassword,
    doUpdatePersonProfile,
    isAdmin,
    doGetPersons,
    doImageChange,
    doRolesChange,
    doPWROverride,
    doChangeStatus,
  };
}
