export interface IAuthPerson {
  person: IPerson;
  accessToken: string;
  expiresIn?: string;
}

export interface IPerson {
  id?: string;
  email?: string;
  name?: string;
  lastnames?: string;
  phone?: string;
  status?: number;
  roles?: number[];
  image?: string;
  dateOfBirth?: string;
  address?: string;
  province?: string;
  city?: string;
  identificationCard?: string;
}

export interface IRegister {
  email: string;
  name: string;
  lastnames: string;
  password: string;
  phone: string;
  dateOfBirth: string;
  address: string;
  province: string;
  city: string;
  identificationCard: string;
}

export interface IRecovery {
  email: string;
}

export interface IPassword {
  password: string;
}

export interface ILogin {
  email: string;
  password: string;
}

export interface IPersonStatus {
  person: IAuthPerson | null;
  error: any;
  loading: boolean;
}

export interface IPet {
  id?: string;
  owner?: string;
  description?: string;
  age?: number;
  weight?: number;
  size?: number;
  gender?: number;
  allergies?: number;
  receivesTreatment?: number;
  chronicIllness?: number;
  vaccinationsReceived?: number;
  sterilized?: number;
  race?: string;
  color?: string;
  allergiesDescription?: string;
  receivesTreatmentDescription?: string;
  chronicIllnessDescription?: string;
  vaccinationsReceivedDescription?: string;
  province?: string;
  city?: string;
  images?: string[];
  image?: any;
  interested?: string[];
  denied?: string[];
  status?: number;
}

export interface IPetStatus {
  error: any;
  loading: boolean;
}

/**
 * Person roles predefined values
 */
export enum ROLES {
  NOT_DEFINED = 0,
  USER = 8,
  ADMIN = 32,
}

/**
 * Pets gender predefined values
 */
export enum PETS_GENDER {
  NOT_DEFINED = 0,
  MALE = 1,
  FEMALE = 2,
}

/**
 * Pet size predefined values
 */
export enum PET_SIZE {
  NOT_DEFINED = 0,
  LITTLE,
  MEDIUM,
  BIG,
}

/**
 * Pets allergies predefined values
 */
export enum PETS_ALLERGIES {
  NOT_DEFINED = 0,
  YES = 1,
  NO = 2,
}

/**
 * Pets treatment predefined values
 */
export enum PETS_TREATMENT {
  NOT_DEFINED = 0,
  YES = 1,
  NO = 2,
}

/**
 * Pets CHRONIC predefined values
 */
export enum PETS_CHRONIC {
  NOT_DEFINED = 0,
  YES = 1,
  NO = 2,
}

/**
 * Pets VACCINATIONS predefined values
 */
export enum PETS_VACCINATIONS {
  NOT_DEFINED = 0,
  YES = 1,
  NO = 2,
}

/**
 * Pets sterilized predefined values
 */
export enum PETS_STERILIZED {
  NOT_DEFINED = 0,
  YES = 1,
  NO = 2,
}

/**
 * Pet state predefined values
 */
export enum PET_CURRENT_STATE {
  SOFT_DELETE = -1,
  BLOCKED,
  ACTIVE,
  PENDING,
  ENDED,
}
