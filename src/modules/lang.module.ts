import { reactive, toRefs } from "vue";

const state = reactive({
  error: null,
  langIso: "en-US",
  loading: false,
});

if (localStorage.getItem("lang_prefs")) {
  state.langIso = JSON.parse(<string>localStorage.getItem("lang_prefs"));
}

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export default function useLang() {
  const doChangeLang = (data: string): Promise<void> => {
    return new Promise<void>((resolve) => {
      localStorage.setItem("lang_prefs", JSON.stringify(data));
      state.langIso = data;
      resolve();
    });
  };

  return {
    ...toRefs(state),
    doChangeLang,
  };
}
