import useAccount from "@/modules/account/account.module";

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const GuardNotAuth = (_to: any, _from: any, next: any) => {
  const { isAuthenticated } = useAccount();
  if (!isAuthenticated()) {
    next();
    return;
  }
  next({ name: "home" });
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const GuardAuth = (to: any, _from: any, next: any) => {
  const { isAuthenticated } = useAccount();
  if (isAuthenticated()) {
    next();
    return;
  }
  next({
    path: "/login",
    query: {
      redirect: to.fullPath,
    },
  });
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const GuardAuthAdmin = (to: any, _from: any, next: any) => {
  const { isAdmin } = useAccount();
  if (isAdmin()) {
    next();
    return;
  }
  next({
    path: "/login",
    query: {
      redirect: to.fullPath,
    },
  });
};
