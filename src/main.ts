import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import booti18n from "@/boot/i18n";
import bootaxios from "@/boot/axios";

bootaxios(booti18n(createApp(App)))
  .use(router)
  .mount("#app");
