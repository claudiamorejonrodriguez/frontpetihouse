import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import MainLayout from "@/layouts/MainLayout.vue";
import Home from "@/views/Home.vue";
import About from "@/views/About.vue";
import Pets from "@/views/Pets.vue";
import PetProfile from "@/views/PetProfile.vue";
import PetError from "@/views/PetError.vue";
import Coming from "@/views/Coming.vue";
import PetDonate from "@/views/PetDonate.vue";
import Login from "@/views/Login.vue";
import Register from "@/views/Register.vue";
import Contact from "@/views/Contact.vue";
import Confirm from "@/views/Confirm.vue";
import Recovery from "@/views/Recovery.vue";
import Password from "@/views/Password.vue";
import { GuardAuth, GuardAuthAdmin, GuardNotAuth } from "@/guards/guards";
import PersonProfile from "@/views/PersonProfile.vue";
import PersonAdmin from "@/views/PersonAdmin.vue";
import PetRegister from "@/views/PetRegister.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    component: MainLayout,
    children: [
      {
        component: Home,
        path: "/",
        name: "home",
      },
      {
        component: About,
        path: "/about",
        name: "about",
      },
      {
        component: Pets,
        path: "/pets",
        name: "pets",
      },
      {
        component: PetProfile,
        path: "/pet-profile",
        name: "pet-profile",
        props: (route) => ({
          ...route.params,
        }),
      },
      {
        component: PetDonate,
        path: "/pet-donate",
        name: "pet-donate",
      },
      {
        component: Login,
        path: "/login",
        name: "login",
        beforeEnter: GuardNotAuth,
      },
      {
        component: Register,
        path: "/register",
        name: "register",
        beforeEnter: GuardNotAuth,
      },
      {
        component: Contact,
        path: "/contact",
        name: "contact",
      },
      {
        component: Confirm,
        path: "/confirm",
        name: "confirm",
        props: (route) => ({
          ...route.params,
        }),
        beforeEnter: GuardNotAuth,
      },
      {
        component: Recovery,
        path: "/recovery",
        name: "recovery",
        beforeEnter: GuardNotAuth,
      },
      {
        component: Password,
        path: "/password",
        name: "password",
        beforeEnter: GuardAuth,
      },
      {
        component: PersonProfile,
        path: "/profile",
        name: "profile",
        beforeEnter: GuardAuth,
      },
      {
        component: PersonAdmin,
        path: "/person-admin",
        name: "person-admin",
        beforeEnter: [GuardAuth, GuardAuthAdmin],
      },
      {
        component: PetRegister,
        path: "/pet-register",
        name: "pet-register",
        props: (route) => ({
          ...route.params,
        }),
        beforeEnter: GuardAuth,
      },
    ],
  },
  {
    component: Coming,
    path: "/coming",
    name: "coming",
  },
  {
    path: "/:catchAll(.*)",
    component: MainLayout,
    children: [
      {
        path: "/:catchAll(.*)",
        component: PetError,
        name: "error404",
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
